<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array( 
    'id'=>'mahasiswa-form', 
    'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>128)); ?>

    <?php echo $form->textFieldRow($model,'nim',array('class'=>'span5','maxlength'=>15)); ?>

    <div class="form-actions"> 
        <?php $this->widget('bootstrap.widgets.TbButton', array( 
            'buttonType'=>'submit', 
            'type'=>'primary', 
            'label'=>$model->isNewRecord ? 'Create' : 'Simpan', 
        )); ?>
    </div> 

<?php $this->endWidget(); ?>