<div class="row">
	<div class="span3 offset1">
		<?php $this->renderPartial('barIndex'); ?>
	</div>
	<div class="span7">
		<div class="well">
			<h1>Edit Tugasakhir</h1>
			<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</div>