<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_mahasiswa',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'judul',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'tahun',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'abstrak',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'deskripsi',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'timestamp',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'poster',array('class'=>'span5','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'download',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
