<div id="tags">
	<?php 
		$tags = Tag::model()->findAll();
		foreach ($tags as $item) {
			$url = array('/tugasakhir/search', 'tag'=>$item->nama);
			if (isset($_GET['q']))
				$url['q'] = $_GET['q'];
			if (isset($_GET['cat']))
				$url['cat'] = $_GET['cat'];
			echo CHtml::link($item->nama, $url, array('class'=>'label'));
			echo " ";
		}
	?>
</div>