<div class="item span3">
	<div class="well">
		<?php echo CHtml::image($data->posterUrl) ?>
		<h5><?php echo CHtml::link($data->judul, array('/tugasakhir/view', 'id'=>$data->id)) ?></h5>
		<?php echo CHtml::link($data->penulis->nama, array('/mahasiswa/view', 'id'=>$data->penulis->id)) ?>
		<p><?php echo $data->abstrak ?></p>
		<?php $mywaktu=new waktu; ?>
		<p>
			<span class="help-inline"><?php echo $mywaktu->nicetime($data->timestamp); ?></span>
			<span style="float:right"><i class="icon icon-arrow-down"></i> <?php echo $data->download ?></span>
		</p>
	
		<?php if ($data->allowEdit()): ?>
			<?php echo CHtml::link('Edit', array('/tugasakhir/update','id'=>$data->id), array('class'=>'btn')) ?> 
			<?php echo CHtml::link('Hapus', array('/tugasakhir/delete','id'=>$data->id), array('class'=>'btn')) ?>
		<?php endif ?>

	</div>
</div>