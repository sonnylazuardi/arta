<div class="row">
	<div class="span3">
		<?php $this->renderPartial('barIndex'); ?>
	</div>
	<div class="span9">
		<h1 class="well">Tugas Akhir</h1>

		<h2>5 TA Terbaru</h2>
		<div class="row">
			<?php $widget = $this->widget('bootstrap.widgets.TbListView',array(
				'dataProvider'=>$dataTerbaru,
				'template'=>'{items}',
				'itemView'=>'_view',
			)); ?>
		</div>
		<h2>5 TA Terpopuler</h2>
		<div class="row">
			<?php $widget = $this->widget('bootstrap.widgets.TbListView',array(
				'dataProvider'=>$dataTerpopuler,
				'template'=>'{items}',
				'itemView'=>'_view',
			)); ?>
		</div>
	</div>
</div>



<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
$(function(){
	var $container = $('.items');

	$container.imagesLoaded( function(){
	  $container.masonry({
	    itemSelector : '.item',
	    isAnimated: true,
    	columnWidth: 240
	  });
	});
});
</script>