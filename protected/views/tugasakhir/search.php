<div class="row">
	<div class="span3">
		<?php $this->renderPartial('barIndex'); ?>
	</div>
	<div class="span9">
		<h1 class="well">Tugas Akhir</h1>
		<div class="row">
			<?php $widget = $this->widget('bootstrap.widgets.TbListView',array(
				'dataProvider'=>$dataProvider,
				'itemView'=>'_view',
				'afterAjaxUpdate' => 'js:function(id, data) {loadMansory();}',
			)); ?>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
$(function(){
	function loadMansory() {
		var $container = $('.items');
		$container.imagesLoaded( function(){
		  $container.masonry({
		    itemSelector : '.item',
		    isAnimated: true,
	    	columnWidth: 240
		  });
		});
	}
	loadMansory();
});
</script>