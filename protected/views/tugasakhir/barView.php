<div class="well">
	<h2 style="text-align:center">Menu</h2>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
  'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
  'stacked'=>true, // whether this is a stacked menu
  'encodeLabel'=>false,
  'items'=>array(
      array('label'=>'Download <span class="badge">'.$model->download.'</span>', 'url'=>array('/tugasakhir/download', 'id'=>$model->id)),
      
  ),
)); ?>
  <?php if ($model->allowEdit()): ?>
      <?php echo CHtml::link('Edit', array('update','id'=>$model->id), array('class'=>'btn')) ?> 
      <?php echo CHtml::link('Hapus', array('delete','id'=>$model->id), array('class'=>'btn')) ?>
    <?php endif ?>
</div>

<div class="well" style="text-align:center">
  <?php echo CHtml::image($model->penulis->profilePicture, null, array('width'=>60)) ?>
  <h4><?php echo CHtml::link($model->penulis->nama, array('/mahasiswa/view', 'id'=>$model->penulis->id)) ?></h4>
  <p class="help-block">
    <?php echo $model->penulis->nim ?> <br/>
    <?php $mywaktu=new waktu; ?>
    <?php echo $mywaktu->nicetime($model->timestamp) ?> <br/>
    <?php echo $model->tahun ?>
  </p>
</div>

<div class="well">
  <h4>Dosen Pembimbing</h4>
  <table class="table">
    <?php 
    $bim = PembimbingTugasakhir::model()->findAll('id_tugasakhir = :i order by posisi asc',array(':i'=>$model->id));
     ?>
    <?php foreach ($bim as $item): ?>
      <tr>
        <td><?php echo $item->posisi ?>.</td>
        <td><?php echo CHtml::image($item->pembimbing->profilePicture, null, array('width'=>30)) ?></td>
        <td><?php echo CHtml::link($item->pembimbing->nama, array('/pembimbing/view', 'id'=>$item->pembimbing->id)) ?></td>
      </tr>  
    <?php endforeach ?>
  </table>
</div>

