<div class="row">
	<div class="span3">
		<?php $data['model'] = $model; ?>
		<?php $this->renderPartial('barView', $data); ?>
	</div>
	<div class="span9">
		<h1 class="well"><?php echo $model->judul ?></h1>
		<a class="media" href="<?php echo $model->linkUrl ?>"></a>
		<hr>
		<div class="row">
			<div class="span4">
				<div class="well">
					<h3>Abstrak</h3>
					<?php echo $model->abstrak ?>
				</div>
			</div>
			<div class="span5">
				<div class="well">
					<h3>Deskripsi</h3>
					<?php echo $model->deskripsi ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.metadata.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.media.js"></script>
<script type="text/javascript">
  $(function() {
      $('a.media').media({width:700, height:900});
  });
</script>



