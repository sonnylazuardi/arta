<div class="well">
	<h2 style="text-align:center">Menu</h2>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
  'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
  'stacked'=>true, // whether this is a stacked menu
  'encodeLabel'=>false,
  'items'=>array(
      array('label'=>'Home', 'url'=>array('/tugasakhir/index')),
      array('label'=>'Pencarian', 'url'=>array('/tugasakhir/search')),
      array('label'=>'Arsip Saya', 'url'=>array('/tugasakhir/arsip'), 'visible'=>Yii::app()->user->checkAccess('mahasiswa')),
      array('label'=>'<i class="icon icon-plus-sign"></i> Tambah', 'url'=>array('/tugasakhir/create'), 'visible'=>Yii::app()->user->checkAccess('mahasiswa')),
  ),
)); ?>
</div>

<div class="well">
  <h2 style="text-align:center">Tag</h2>
  <?php $this->renderPartial('tag') ?>
</div>