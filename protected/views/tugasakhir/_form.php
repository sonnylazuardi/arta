<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tugasakhir-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'judul',array('class'=>'input-block-level','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'tahun',array('class'=>'input-block-level')); ?>

	<?php echo $form->textFieldRow($model,'abstrak',array('class'=>'input-block-level','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'deskripsi',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

	<?php echo $form->fileFieldRow($model,'link',array('class'=>'input-block-level','maxlength'=>128)); ?>

	<?php echo $form->fileFieldRow($model,'poster',array('class'=>'input-block-level','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'mytag',array('class'=>'input-block-level','id'=>'mytag')); ?>

	<?php echo $form->dropDownListRow($model,'mypembimbing',CHtml::listData(Pembimbing::model()->findAll(), 'id', 'nama'),array('class'=>'input-block-level', 'id'=>'mypembimbing', 'multiple'=>'multiple')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<div class="clear"></div>

<link href="<?php echo Yii::app()->baseUrl ?>/css/jquery.autocomplete.css" rel="stylesheet">

<link href="<?php echo Yii::app()->baseUrl ?>/css/jquery-ui.css" rel="stylesheet">
<link href="<?php echo Yii::app()->baseUrl ?>/css/jquery.bsmselect.css" rel="stylesheet">



<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.autocomplete.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.bsmselect.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.bsmselect.sortable.js"></script>

<script type="text/javascript">
$(function() {
 	 var last_valid_selection = null;

  $("#mytag").autocomplete("<?php echo Yii::app()->createAbsoluteUrl('tugasakhir/tags') ?>", {
    remoteDataType: 'json', minChars: 1, useDelimiter: true
  });

 //  $(".chzn-select").chosen({ max_selected_options: 3 });
 
	// var ret = [];
 //  $(".chzn-select").find('.search-choice').each(function(){
 //    var selectedValue = $(this).find('span').text();
 //    ret.push(selectedValue);  
 //    console.log(selectedValue);
 //  });
	$("#mypembimbing").bsmSelect({
	  showEffect: function($el){ $el.fadeIn(); },
	  hideEffect: function($el){ $el.fadeOut(function(){ $(this).remove();}); },
	  plugins: [$.bsmSelect.plugins.sortable()],
	  title: 'Pilih Dosen Pembimbing',
	  highlight: 'highlight',
	  addItemTarget: 'original',
	  removeLabel: '<strong>X</strong>',
	  containerClass: 'bsmContainer',                // Class for container that wraps this widget
	  listClass: 'bsmList-custom',                   // Class for the list ($ol)
	  listItemClass: 'bsmListItem-custom',           // Class for the <li> list items
	  listItemLabelClass: 'bsmListItemLabel-custom', // Class for the label text that appears in list items
	  removeClass: 'bsmListItemRemove-custom',       // Class given to the "remove" link
	  extractLabel: function($o) {return $o.html();}
	});
});
</script>

