<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row">
	<div class="span6 offset3">
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
			<div class="well form">

				<h1 style="text-align:center">Login</h1>
				<?php echo $form->textFieldRow($model,'username', array('class'=>'input-block-level')); ?>

				<?php echo $form->passwordFieldRow($model,'password', array('class'=>'input-block-level')); ?>

				<?php echo $form->checkBoxRow($model,'rememberMe'); ?>
				
				<div class="form-actions">
					<?php $this->widget('bootstrap.widgets.TbButton', array(
			            'buttonType'=>'submit',
			            'type'=>'primary',
			            'size'=>'large',
			            'label'=>'Login',
			        )); ?>
				</div>
			</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

