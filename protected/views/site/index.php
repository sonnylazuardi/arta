	<div class="row">
		<div class="span10 offset1 headline">	
			<p><b>ARTA</b> (Arsip Tugas Akhir) merupakan sebuah portal sebagai tempat repository tugas akhir mahasiswa Informatika Institut Teknologi Bandung. Portal ini dibentuk demi tercapainya kebutuhan untuk mengumpulkan serta memberi kemudahan dalam mencari tugas akhir mahasiswa yang ingin diakses.</p>
		</div>
	</div>
	<?php $this->renderPartial('//tugasakhir/tag') ?>
	<div class="row">
		<?php $widget = $this->widget('bootstrap.widgets.TbListView',array(
			'dataProvider'=>$dataProvider,
			'id'=>'tugas-akhir',
			'template'=>'{items}{pager}',
			'itemView'=>'//tugasakhir/_view',
			'pager' => array(
        'class' => 'ext.infiniteScroll.IasPager', 
        'rowSelector'=>'.item', 
        'listViewId' => 'tugas-akhir', 
        'header' => '',
        'loaderText'=>'Loading...',
        'options' => array('history' => false, 'triggerPageTreshold' => 2, 'trigger'=>'Load more'),
      ),      
		)); ?>
	</div>


<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
$(function(){
	var $container = $('.items');

	$container.imagesLoaded( function(){
	  $container.masonry({
	    itemSelector : '.item',
	    isAnimated: true,
    	columnWidth: 240
	  });
	});
});
</script>