<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA : Arsip Tugas Akhir</title>
	<style>
          body { background: black; }
          .media {background : black;}
    </style>
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/bootstrap.css">

	
	<div class="media" >
		<a class="pull-left" href="#">
		<?php echo CHtml::image(Yii::app()->request->baseUrl . '/img/arta.png') ?>
		</a>
		<div class="media" style="margin-top:50px">
			<h5 class="media-heading"> <font color="white" face="century gothic">Repository Tugas Akhir Mahasiswa Informatika Institut Teknologi Bandung</font></h5>
		</div>
		<div class="media">
					<ul id="myTab" class="nav nav-tabs bwh stripb">
					<li><a href="<?php echo Yii::app()->createUrl('/site/index') ?>" data-toggle="tab"><i class="icon-home"></i> Home</a></li>
					<li><a href="#arsip" data-toggle="tab"><i class="icon-folder-open"></i> Arsip</a></li>
					<li><a href="#profile" data-toggle="tab"><i class="icon-eye-open"></i> Profile</a></li>
					<li><a href="<?php echo Yii::app()->createUrl('/site/about') ?>" data-toggle="tab"><i class="icon-hand-left"></i> About Us</a></li>
					<li><a href="<?php echo Yii::app()->createUrl('/site/contact') ?>" data-toggle="tab"><i class="icon-envelope"></i> Contact</a></li>
					<li><a href="<?php echo Yii::app()->createUrl('/site/login') ?>" data-toggle="tab"><i class="icon-user"></i> Login</a></li>
            </ul>
		</div>
	</div>
				
	
	
	
	<div class="strip" style="margin-top:30px">
      <div class="container">
          <div class="full-search-bar" align="right">
            <form class="form-search">
				<input type="text" class="input-medium search-query">
				<button type="submit" class="btn btn-info"><i class="icon-search"></i>Pencarian Tugas Akhir</button>
			</form>
          </div>  
		 </div>
	</div>
	
</head>
<body>
	
		<div class="container">
			<div id="content">
				<?php echo $content; ?>
			</div><!-- content -->
		</div>
	
	
</body>
<footer class="footer">
  <div class="media" style="margin-top:50px">
			<h5 class="media-heading" style="margin-top:50px"> <font color="white" face="century gothic">ARTA, Arsip Tugas Akhir &copy; Basis Data 2013.</font></h5>
	</div>
</footer>
</html>