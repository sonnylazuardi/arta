<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

		 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/styles.css" />
		 
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-responsive.css" />
		
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
		'type'=>'inverse',
		'brand'=>'Arta',
    'items'=>array(
    		$this->renderPartial('//layouts/search-form', null, true),
    // 		array(
    //     	'class'=>'bootstrap.widgets.TbMenu',
    //     	'encodeLabel'=>false,
    //       'items'=>array(
    //       	array('label'=>' ', 'items'=>array(
				// 			array('label'=>$this->renderPartial('//layouts/search-cat', null, true)),
				// 		)),
				// 	),
				// ),
        array(
        	'class'=>'bootstrap.widgets.TbMenu',
        	'htmlOptions'=>array('class'=>'pull-right'),
        	'encodeLabel'=>false,
          'items'=>array(
          	array('label'=>'<i class="icon-book icon-white"></i> Jelajahi', 'url'=>array('/tugasakhir/index')),
          	array('label'=>'<i class="icon-plus-sign icon-white"></i> Buat Akun', 'visible'=>Yii::app()->user->isGuest, 'items'=>array(
							array('label'=>'Mahasiswa', 'url'=>array('/mahasiswa/register')),	
							array('label'=>'Pembimbing', 'url'=>array('/pembimbing/register')),	
						)),
						array('label'=>'<i class="icon-user icon-white"></i> Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>Yii::app()->user->name, 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
							array('label'=>'Profile', 'url'=>Yii::app()->user->profileUrl),
							array('label'=>'Logout', 'url'=>array('/site/logout')),	
						)),
					),
				),
    ),
)); ?>
<div class="container" id="page">

	<?php $this->widget('bootstrap.widgets.TbAlert'); ?>

	<?php echo $content; ?>

</div><!-- page -->

<div class="clear"></div>

<div id="footer">
	Arsip Tugas Akhir Basis Data &copy; <?php echo date('Y'); ?>
</div><!-- footer -->

</body>
</html>
