<form class="navbar-search pull-left" action="<?php echo Yii::app()->createUrl('/tugasakhir/search') ?>">
	<input type="search" class="span3" placeholder="Cari Tugas Akhir" name="q" value="<?php echo (isset($_GET['q']) ? $_GET['q'] : '') ?>" id="search">

	<?php if (isset($_GET['tag'])): ?>
		<input type="hidden" name="tag" value="<?php echo $_GET['tag'] ?>">
	<?php endif ?>
	<?php $this->renderPartial('//layouts/search-cat') ?>
</form>

<link href="<?php echo Yii::app()->baseUrl ?>/css/jquery.autocomplete.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/js/jquery.autocomplete.js"></script>
<script type="text/javascript">
$(function() {
  $("#search").autocomplete('<?php echo Yii::app()->createAbsoluteUrl("/tugasakhir/search") ?>' + $("#cat").val().toString(), {
    remoteDataType: 'json', minChars: 2,
    showResult: function(value, data) {
      return "<img src=\"" + data[2] + "\" width=\"50px\" class=\"sauto\"/> <b>" + value + "</b> <br>" + data[1];
    },
    onItemSelect: function(item) {
      window.location = "<?php echo Yii::app()->createAbsoluteUrl('/tugasakhir/view/id') ?>/" + item.data[0];
    },
  });
});
</script>