<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pembimbing-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'input-block-level','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'nip',array('class'=>'input-block-level','maxlength'=>20)); ?>

	<?php echo $form->textAreaRow($model,'riwayat_pendidikan',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

	<?php echo $form->textAreaRow($model,'publikasi',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

	<?php echo $form->textAreaRow($model,'research',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

	<?php echo $form->textAreaRow($model,'bidang_keahlian',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

	<?php // echo $form->textFieldRow($model,'foto',array('class'=>'input-block-level','maxlength'=>128)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
