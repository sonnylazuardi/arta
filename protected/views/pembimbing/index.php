<?php
$this->breadcrumbs=array(
	'Pembimbings',
);

$this->menu=array(
	array('label'=>'Create Pembimbing','url'=>array('create')),
	array('label'=>'Manage Pembimbing','url'=>array('admin')),
);
?>

<h1>Pembimbings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
