<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip')); ?>:</b>
	<?php echo CHtml::encode($data->nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('riwayat_pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->riwayat_pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publikasi')); ?>:</b>
	<?php echo CHtml::encode($data->publikasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('research')); ?>:</b>
	<?php echo CHtml::encode($data->research); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang_keahlian')); ?>:</b>
	<?php echo CHtml::encode($data->bidang_keahlian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foto')); ?>:</b>
	<?php echo CHtml::encode($data->foto); ?>
	<br />

	*/ ?>

</div>