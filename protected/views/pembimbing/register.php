<?php
/* @var $this PembimbingFormController */
/* @var $model PembimbingForm */
/* @var $form CActiveForm */
?>
<div class="row">
	<div class="span6 offset3">
		<div class="well">
			<h1>Daftar Pembimbing</h1>
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id'=>'pembimbing-form-register-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>

			<?php //echo $form->errorSummary($model); ?>

				<?php echo $form->textFieldRow($model,'username', array('class'=>'input-block-level')); ?>

				<?php echo $form->textFieldRow($model,'email', array('class'=>'input-block-level')); ?>

				<?php echo $form->passwordFieldRow($model,'password', array('class'=>'input-block-level')); ?>

				<?php echo $form->passwordFieldRow($model,'password2', array('class'=>'input-block-level')); ?>

				<?php echo $form->textFieldRow($model,'nama',array('class'=>'input-block-level','maxlength'=>128)); ?>

				<?php echo $form->textFieldRow($model,'nip',array('class'=>'input-block-level','maxlength'=>20)); ?>

				<?php echo $form->textAreaRow($model,'riwayat_pendidikan',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

				<?php echo $form->textAreaRow($model,'publikasi',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

				<?php echo $form->textAreaRow($model,'research',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

				<?php echo $form->textAreaRow($model,'bidang_keahlian',array('rows'=>6, 'cols'=>50, 'class'=>'input-block-level')); ?>

				<?php echo $form->fileFieldRow($model,'foto'); ?>
				
				<?php if(CCaptcha::checkRequirements()): ?>
					<?php echo $form->captchaRow($model,'verifyCode'); ?>
				<?php endif; ?>

				<div class="form-actions">
					<?php $this->widget('bootstrap.widgets.TbButton',array(
			            'buttonType'=>'submit',
			            'type'=>'primary',
			            'size'=>'large',
			            'label'=>'Daftar',
			        )); ?>
				</div>

		<?php $this->endWidget(); ?>
		</div>
	</div>	
</div>