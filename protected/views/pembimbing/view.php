<div class="row">
	<div class="span6 offset3">
		<div class="well">
			<h1 style="text-align:center">Profile</h1>
			
			<div style="margin:20px; text-align:center">
				<?php echo CHtml::image($model->profilePicture) ?>
			</div>

			<?php $this->widget('bootstrap.widgets.TbDetailView', array(
				'data'=>$model,
				'attributes'=>array(
					'nama',
					'nip',
					'riwayat_pendidikan',
					'publikasi',
					'research',
					'bidang_keahlian',
					'foto',
					array(
					  'label'=>'email',
					  'value'=>$model->user->email,
					),
				),
			)); ?>

			<?php if (Yii::app()->user->allowEdit('pembimbing', $model->id)): ?>
				<?php echo CHtml::link('Edit Profil', array('update', 'id'=>$model->id), array('class'=>'btn')) ?>
			<?php endif ?>
		</div>
	</div>
</div>
