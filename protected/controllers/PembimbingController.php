<?php

class PembimbingController extends Controller
{
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'foreColor'=>0xff755a,
			),
		);
	}
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','register','captcha'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pembimbing;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pembimbing']))
		{
			$model->attributes=$_POST['Pembimbing'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pembimbing']))
		{
			$model->attributes=$_POST['Pembimbing'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pembimbing');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pembimbing('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pembimbing']))
			$model->attributes=$_GET['Pembimbing'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Pembimbing::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pembimbing-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	/**
	 * Melakukan Registrasi User untuk Pembimbing
	 */
	public function actionRegister()
	{
	    $model=new PembimbingForm('register');

	    if(isset($_POST['PembimbingForm']))
	    {
        $model->attributes=$_POST['PembimbingForm'];
        if($model->validate())
        {
          $model2 = new User;    
          $model2->scenario = 'register';
          $model2->attributes = $_POST['PembimbingForm'];
          $model2->salt = $model2->generateSalt();
          $model2->password = $model2->hashPassword($model2->password, $model2->salt);
          $model2->role = 'pembimbing';

          if ($model2->save()) {
          	$model3 = new Pembimbing;
          	$model3->scenario = 'register';
          	$model3->attributes = $_POST['PembimbingForm'];
          	$model3->id_user = $model2->id;
          	$model3->foto = CUploadedFile::getInstance($model,'foto');
						if ($model3->save()) {
							if ($model3->foto) {
								$path = getcwd().'/images/pembimbing/'.$model3->foto;
								$model3->foto->saveAs($path);
								Yii::app()->phpThumb->makeThumb($path);
						  }
							Yii::app()->user->setFlash('success','Registrasi pembimbing berhasil');
         			$this->redirect(array('/site/index'));
						} else {
							$model2->delete();
							throw new CHttpException(505, "Kesalahan dalam insert ".CHtml::errorSummary($model3));
						}
          } else throw new CHttpException(505, "Kesalahan dalam insert ".CHtml::errorSummary($model2));
       	}
	    }
	    $this->render('register',array('model'=>$model));
	}
}
