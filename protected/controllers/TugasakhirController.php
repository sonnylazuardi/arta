<?php

class TugasakhirController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','search','tags','pembimbing','download', 'search0','search1','search2'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','admin','arsip'),
				'roles'=>array('mahasiswa'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Tugasakhir('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tugasakhir']))
		{
			$model->attributes=$_POST['Tugasakhir'];
			$model->poster = CUploadedFile::getInstance($model, 'poster');
			$model->link = CUploadedFile::getInstance($model, 'link');
			if($model->save()) {
				if ($model->poster) {
					$path = getcwd().'/images/poster/'.$model->poster;
					$model->poster->saveAs($path);
					Yii::app()->phpThumb->makeThumb($path, 180, 227);
				}
				if ($model->link) {
					$path = getcwd().'/upload/pdf/'.$model->link;
					$model->link->saveAs($path);
				}
				$model->savePembimbing();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		if (!$model->allowEdit())
			throw new CHttpException(403, 'Anda tidak berhak mengedit TA ini');
		$model->scenario = 'update';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tugasakhir']))
		{
			$model->attributes=$_POST['Tugasakhir'];
			if (is_object(CUploadedFile::getInstance($model, 'poster')))
				$model->poster = CUploadedFile::getInstance($model, 'poster');
			if (is_object(CUploadedFile::getInstance($model, 'link')))
				$model->link = CUploadedFile::getInstance($model, 'link');
			if($model->save()) {
				if (is_object($model->poster)) {
					$path = getcwd().'/images/poster/'.$model->poster;
					$model->poster->saveAs($path);
					Yii::app()->phpThumb->makeThumb($path, 180, 227);
				}
				if (is_object($model->link)) {
					$path = getcwd().'/upload/pdf/'.$model->link;
					$model->link->saveAs($path);
				}
				$model->savePembimbing();
				$this->redirect(array('view','id'=>$model->id));
			}
		} else {
			$model->loadTags();
			$model->loadPembimbing();
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		// if(Yii::app()->request->isPostRequest)
		// {
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			if (!$model->allowEdit())
				throw new CHttpException(403, 'Anda tidak berhak mengedit TA ini');
			$model->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		// }
		// else
		// 	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$cBaru = new CDbCriteria;
		$cBaru->order = 'timestamp desc';
		$cBaru->limit = 5;
		$cPopuler = new CDbCriteria;
		$cPopuler->order = 'download desc';
		$cPopuler->limit = 5;
		$data['dataTerbaru']=new CActiveDataProvider('Tugasakhir', array('criteria'=>$cBaru,'pagination'=>false));
		$data['dataTerpopuler']=new CActiveDataProvider('Tugasakhir', array('criteria'=>$cPopuler,'pagination'=>false));
		$this->render('index',$data);
	}

	/**
	 * Pencarian models.
	 */
	public function actionSearch($tag = '', $q = '', $cat = '')
	{
		$c = new CDbCriteria;
		if (!empty($tag)) {
			$c->with = 'tags';
			$c->together = true;
			$c->compare('tags.nama', $tag);
		}
		if (!empty($q)) {
			$arrayCat = array(0=>'judul',1=>'penulis.nama',2=>'pembimbing.nama');
			$exp = explode(" ", $q);
			$c->with = array('tags', 'penulis', 'pembimbing');
			$c->together = true;
			if (array_key_exists($cat, $arrayCat)) {
				foreach ($exp as $token) {
					$token = trim($token);
					if (!empty($token))
						$c->compare($arrayCat[$cat], $token, true, 'or');
				}
			}
		}
		$dataProvider=new CActiveDataProvider('Tugasakhir', array('criteria'=>$c, 'pagination'=>array('pageSize'=>8)));
		$this->render('search',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * List arsip tugas akhir yang sedang login
	 */
	public function actionArsip()
	{
		$c = new CDbCriteria;
		$c->compare('id_mahasiswa', Yii::app()->user->realId);
		$dataProvider=new CActiveDataProvider('Tugasakhir', array('criteria'=>$c, 'pagination'=>array('pageSize'=>8)));
		$this->render('arsip',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Tugasakhir('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tugasakhir']))
			$model->attributes=$_GET['Tugasakhir'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Tugasakhir::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tugasakhir-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionTags($q)
	{
		$model = Tag::model()->findAll('nama like :q', array(':q'=>strtolower($q)."%"));
		$arr = array();
		foreach ($model as $item) {
			$arr[] = $item->nama;
		}
		echo json_encode($arr);
	}

	public function actionPembimbing($q)
	{
		$model = Pembimbing::model()->findAll('nama like :q', array(':q'=>"%".strtolower($q)."%"));
		$arr = array();
		foreach ($model as $item) {
			$arr[] = $item->nama;
		}
		echo json_encode($arr);
	}

	public function actionSearch0($q)
	{
		$model = Tugasakhir::model()->findAll('judul like :q', array(':q'=>"%".strtolower($q)."%"));
		$arr = array();
		foreach ($model as $item) {
			$arr[] = array($item->judul, $item->id, $item->penulis->nama, $item->posterUrl);
		}
		echo json_encode($arr);
	}
	public function actionSearch1($q)
	{
		$model = Tugasakhir::model()->with('penulis')->findAll('penulis.nama like :q', array(':q'=>"%".strtolower($q)."%"));
		$arr = array();
		foreach ($model as $item) {
			$arr[] = array($item->judul, $item->id, $item->penulis->nama, $item->posterUrl);
		}
		echo json_encode($arr);
	}
	public function actionSearch2($q)
	{
		$model = Tugasakhir::model()->with('pembimbing')->findAll('pembimbing.nama like :q', array(':q'=>"%".strtolower($q)."%"));
		$arr = array();
		foreach ($model as $item) {
			$arr[] = array($item->judul, $item->id, $item->penulis->nama, $item->posterUrl);
		}
		echo json_encode($arr);
	}
	public function actionDownload($id)
	{
		$model = $this->loadModel($id);
		$model->setScenario('download');
		$model->download++;
		if (!$model->save())
			throw new CHttpException(403, CHtml::errorSummary($model));
		$this->redirect(Yii::app()->createAbsoluteUrl('/upload/pdf/'.$model->link));
	}
}
