-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2013 at 03:03 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arta`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE DATABASE `arta`;

USE `arta`;

DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `foto` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `id_user`, `nama`, `nim`, `foto`) VALUES
(8, 13, 'Bernadette Vina', '18211019', '552060_10151152069096838_508537115_n.jpg'),
(9, 26, 'Sonny Lazuardi Hermawan', '13511029', '184869_1724560206667_652612_n.jpg'),
(10, 27, 'Arini Hasiana', '13511082', '984124_4913862689832_1370743829_n.jpg'),
(11, 28, 'Farizan Ramadhan', '13511081', '536154_4933256372721_866196574_n.jpg'),
(12, 29, 'Stella Kurniawan', '18211046', '537209_4513465352812_473426009_n.jpg'),
(13, 30, 'William Gumarus', '18211043', '377817_3798812289132_422775949_n.jpg'),
(14, 31, 'Fransiskus Xaverius Christian', '13511016', '539806_10151457219724516_1873264206_n.jpg'),
(15, 32, 'Ignatius Evan Daryanto', '13511019', '936764_470108359742627_910754908_a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembimbing`
--

DROP TABLE IF EXISTS `pembimbing`;
CREATE TABLE IF NOT EXISTS `pembimbing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `riwayat_pendidikan` text,
  `publikasi` text,
  `research` text,
  `bidang_keahlian` text,
  `foto` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_pembimbing` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `pembimbing`
--

INSERT INTO `pembimbing` (`id`, `id_user`, `nama`, `nip`, `riwayat_pendidikan`, `publikasi`, `research`, `bidang_keahlian`, `foto`) VALUES
(5, 14, 'Benhard Sitohang', '195407161980111 00', 'Undergraduate: Electrical Engineering, Institut Teknologi Bandung, Bandung - Indonesia;\r\n\r\nMagister: Conception Assistee Des Systemes, USTL-Montpellier II, Montpellier - France;\r\n\r\nDoctoral: Automatique, Universite de Sciences et Techniques et Languedoc-Montpellier II, Montpellier - France', '"Measurement of the Quality of an FCO-IM Conceptual Data Model", International Journal on Electrical Engineering and Informatics - Volume 2, Number 4, 2010\r\n\r\n"The concept of Data Model Patern Based on Fully Communication", Journal of Information and Communication Technology, ITB\r\n\r\n"Information Grammar for Patterns (IGp) for Pattern Language of Data Model Patterns Based on Fully Communication Oriented Information Modeling (FCO-IM)", The 2010 Object Role Modeling (ORM) Workshop, Kreta - Yunani, 26 - 29 Oktober 2010\r\n\r\n"A pattern language of conceptual data model patterns based on FCO_IM", International Arab Conference on Information Technology (ACII), Benghazy, Lybia, July 2010\r\n\r\n"Perkembangan dalam Bidang Pencarian Berbasis Konten", Konferensi Nasional Informatika 2010, Institut Teknologi Bandung, 11 Oktober 2010\r\n\r\n"PENCARIAN BERBASIS KONTEN: STUDI KASUS SISTEM IDENTIFIKASI OTOMATIS SIDIK JARI", Prosiding Konferensi Nasional Teknologi Informasi dan Komunikasi Universitas Kristen Duta Wacana 2010, ISBN: 978-602-95792-0-8\r\n"AN ALGORITHM FOR MINING ASSOCIATION RULE IN SPATIO-TEMPORAL DATA", Proceeding of the 5 th International Conference on Information & Communication Technology and Systems 2009 (ICTS2009)\r\n\r\n"The Analysis and Application of Classification Methods for Software Development Non-Tulis University Student Enrollment System (Case Study at IT TELKOM)", International Conference on Telecommunication (ICTEL 2009)\r\n"Generic Data Model Patterns using Fully Communication Oriented Information Modelling (FCO-IM)", International Journal on Electrical Engineering and Informatics - Volume 1, Number 1, 2009\r\n\r\n"Open Source & Specification of Software (OS&SS) untuk Meningkatkan Produktifitas Pengembangan Perangkat Lunak", Konferensi dan Temu Nasional Teknologi Informasi & Komunikasi (TIK) untuk Indoensia 2008 (eII 2008) V.71, Jakarta, May 21-23, 2008\r\n\r\n"Term of Reference Seminar “Open Source, a New Way for Education”", Seminar HM IF ITB, 20 September 2008\r\n"Combining PageRank and Cititation Analysis to Measure Information Credibility in Internet", Proceedings of the 9th International Conference on Information Integration and Web-based Application & Services (iiWAS2007) December 2007\r\n\r\n"Improvement of CB & BC Algorithms (CB* Algorithm) for Learning Structure of Bayesian Networks as Classier in Data Mining", Proceeding ITB, September 2008\r\n\r\n"Aspek Motivasi Pada Sistem e-Learning", Konferensi Nasional Teknologi Informasi dan Komunikasi untuk Indonesia (eII2007), Jakarta, 25-26 April 2007\r\n\r\n"Desain Metode Prapemrosesan Data Spatio-Temporal untuk Spatio-Temporal Data Mining", National Conference on Computer Science & Information Technology 2007, Depok, 29-30 January 2007\r\n\r\n"Memilih CMS Open Source", Seminar Nasional Open Source Software ke-2, Bandung, 8 Agustus 2007\r\n\r\n"Mengukur Kredibilitas Informasi di Internet", National Conference on Computer Science & Information Technology 2007, Depok, 29-30 January 2007\r\n\r\n"Combining Pagerank and Citation Analysis to Measure Information Credibility in Internet", The 9th International Conference on Information Integration and Web-based Applications Services (iiWAS 2007), Jakarta, 3-5 December 2007\r\n\r\n"Empowerment of Open Source Software in Indonesia", Asia Open Source Software Conference and Showcase, Bangkok 7-8 November 2007\r\n\r\n"Implementing Bayes Method in E-Learning System to Stimulate Student Motivation", International Conference on Rural Information and Communication Technology (r-ICT 2007), Bandung, 5-6 August, 2007\r\n\r\n"Implementing PageRank to Measure Information Credibility in Internet", International Conference on Rural Information and Communication Technology (r-ICT 2007), Bandung, 5-6 August, 2007\r\n\r\n"Spatial Data Preprocessing for Mining Spatial Association Rule with Conventional Association Mining Algorithms", The International Conference on Electrical Engineering and Informatics (ICEEI 2007), Bandung, 17-19 June 2007\r\n\r\n"Coalescing Pada Basis Data Bitemporal", Jurnal Piranti Warta, Universitas Bina Nusantara, 2007\r\n\r\n"IQMS: Peningkatan Derajat Konkurensi Eksekusi Query", Jurnal GEMATIKA, Desember 2007\r\n\r\n"PQL: Konversi Sintaks SQL Menjadi Sintaks PQL", Jurnal Informatika, Universitas Petra Surabaya, November 2007\r\n\r\n"Sistem Rekomendasi Pemilihan Citra Batik: Teknik Klassifikasi Berdasarkan Motif", Jurnal Ilmiah Ilmu Komputer, Universitas Pelita Harapan, Vol.5, No.3, September 2007\r\n\r\n"Online Electronic Learning Berbasis Konstruktivistik", GEMATIKA Vol.8 No.1, Jurnal Manajemen Informatika, 2006\r\n"Kredibilitas Informasi di Internet", GEMATIKA Vol.8 No.1, Jurnal Manajemen Informatika, 2006\r\n\r\n"TPDA Algorithm for Learning BN Structure from Missing Value and Outliers in Data Mining", Jurnal Informatika Pusat Penelitian, 2006\r\n\r\n"Searching Object-Relational DBMS Features for Improving Efficiency and Scalability of Decision Tree Algorithms", International Conference iiWAS 2006 (submission to Masters and Doctoral Colloqium of iiWAS)\r\n\r\n"Semi-supervised Clustering Method for Comparative Text Mining", The 2nd Information and Communication Technology Seminar, 2006', '', 'Database Systems', 'benhard3x4.jpg'),
(6, 15, 'Tricya Esterina Widagdo', '19710907199702200', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Computer Science, University of Otago, Dunedin - New Zealand', '"Penggunaan Teknologi Semantik untuk Pengembangan Sistem Penyimpanan Data yang Berintelejensi – Studi Kasus e-Health", e-Indonesia Initiative 2009 (eII2009) Konferensi dan Temu Nasional Teknologi Informasi dan Komunikasi untuk Indonesia 24 - 25 Juni 2009, Bandung\r\n\r\n"Managing Referential Integrity in Bitemporal Databases", International Conference on Rural Information and Communication Technology (r-ICT 2007), Bandung, 5-6 August, 2007\r\n\r\n"Coalescing Pada Basis Data Bitemporal", Jurnal Piranti Warta, Universitas Bina Nusantara, 2007\r\n\r\n"PQL: Konversi Sintaks SQL Menjadi Sintaks PQL", Jurnal Informatika, Universitas Petra Surabaya, November 2007\r\n\r\n"Pengelolaan Basis Data Bitemporal Memanfaatkan Fasilitas Pemrograman Basis Data", Seminar Riset Teknologi Informasi 2006', '', 'Database', 'cia3x4.jpg'),
(7, 16, 'Rinaldi Munir', '132084796', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia', 'Derivation of Barni Algorithm into Its Asymmetric Watermarking Technique Using Statistical Approach", International Journal on Electrical Engineering and Informatics - Volume 1, Number 2, 2009\r\n\r\n"Derivation of Barni Algorithm into Its Asymmetric Watermarking Technique Using Statistical Approach", International Conference on Electrical Engineering nad Informatics 2009 (ICEEI 2009) Universiti Kebangsaan Malaysia, Bangi Selangor 5-7 Agustus 2009\r\n\r\n"A Public-Key Watermarking Method for Still Images Based on Random Permutation", Proceeding of the 2008 International Joint Conference in Engineering IJSE2008, August 4-5, Jakarta, Indonesia\r\n\r\n"An Asymmetric Watermarking Method in the DCT Domain Based on RC4-Permutation and Chaotic Map", Jurnal ICT ITB 2008\r\n\r\n"Metode Asymmetric Watermarking pada Citra Digital Berbasiskan pada Permutasi-RC4 dan Fungsi Chaos", Seminar on Intelligent Technology and Its Applications 2008 ISBN 978-979-8897-24-5\r\n\r\n"PENURUNAN ALGORITMA SYMMETRIC WATERMARKING MENJADI ALGORITMA ASYMMETRIC WATERMARKING STUDI KASUS: ALGORITMA BARNI", Seminar Nasional Ilmu Komputer dan Aplikasinya – SNIKA 2008 (27/11/2008)\r\n\r\n"Skema Asymmetric Watermarking Berbasiskan Uji Korelasi", GEMATIKA JURNAL MANAJEMEN INFORMATIKA, VOLUME 9 NOMOR 1, DESEMBER 2007\r\n\r\n"Metode Asymetric Watermarking Dengan Penjumlahan Chaos dalam Ranah DCT", Seminar Nasional Ilmu Komputer dan Aplikasinya (SNIKA 2007), 22 November 2007\r\n\r\n"Modifikasi Spread Spectrum Watermarking dari Cox Berbasiskan pada Enkripsi Chaotic", The 8th Seminar on Intelligent Technology and Its Applications (SITIA), Surabaya, 9 Mei 2007\r\n\r\n"Penyembunyian Data Secara Aman Di Dalam Citra Berwarna Dengan Metode LSB Jamak Berbasis Chaos", Seminar Nasional Ilmu Komputer dan Aplikasinya (SNIKA 2007), 22 November 2007\r\n\r\n"Metode Blind Image-Watermarking Berbasis Chaos dalam Ranah Discrete Cosine Transform (DCT)", The International Conference on Electrical Engineering and Informatics (ICEEI 2007), Bandung, 17-19 June 2007)\r\n\r\n"Secure Spread Spectrum Watermarking Algorithm Based on Chaotic Map for Still Images", The International Conference on Electrical Engineering and Informatics (ICEEI 2007), Bandung, 17-19 June 2007)\r\n\r\n"The Balinese Unicode Text Processing", Indonesia Journal Of Computing and Cybernetic Systems, Vol.1, No.1, Februari 2007\r\n\r\n"Aplikasi Image Thresholding untuk Segmentasi Objek", Seminar Nasional Aplikasi Teknologi Informasi 2006\r\n"Restorasi Citra Kabur dengan Algoritma Lucy-Richardson dan Perbandingannya dengan Penapis Wiener", Seminar Nasional Aplikasi Teknologi Informasi 2006\r\n\r\n"Sekilas Image Watermarking untuk Memproteksi Citra Digital dan Aplikasinya pada Citra Medis", Seminar Indonesia Conference on Telecommunications 2006\r\n\r\n"Penerapan Sistem Kriptografi Kunci-Publik untuk Membentuk Skema Public-Key Watermarking, Mungkinkah ?", Seminar Nasional Aplikasi Teknologi Informasi 2006\r\n\r\n"Perancangan Algortima Kriptografi Stream Cipher dengan Chaos", Proc. Konferensi Nasional Sistem Informasi 2006\r\n"Review Skema Public-Key Watermarking untuk Proteksi Copyright pada Distribusi Produk Multimedia", Proc. Of The 17th Seminar on Intelligent Technology and Its Applications 2006', '', 'Kriptografi dan Security; Pengolahan Citra; Metode Numerik; Matemetika Diskrit', 'rinaldi3x4.jpg'),
(8, 17, 'Saiful Akbar', '19740509199803100', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nDoctoral: University of Linz, Austria', '"e-Health Implementation for Rural Areas in Indonesia - Review on Data and Technology Readiness", r-ICT Bandung Indonesia, 17-18 Juni 2009. ISBN : 978-979-15509-4-9.\r\n\r\n"Penggunaan Teknologi Semantik untuk Pengembangan Sistem Penyimpanan Data yang Berintelejensi – Studi Kasus e-Health", e-Indonesia Initiative 2009 (eII2009) Konferensi dan Temu Nasional Teknologi Informasi dan Komunikasi untuk Indonesia 24 - 25 Juni 2009, Bandung.\r\n\r\n"Multishape-features and text-feature integration on 3D model similarity retrieval", International Journal Innovative Computing and Applications, Vol. 1, No. 3, 2008', '', 'Multimedia Database and Retrieval; Information Extraction and Visualization', NULL),
(9, 18, 'Riza Satria Perdana', '19700609199512100', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\nPublication', '', '', '', 'riza3x4.jpg'),
(10, 19, 'Ayu Purwarianti', '19770127200801201', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nDoctoral: Toyohashi University of Technology', '"Study of Non Factoid Question Answering System for Bahasa Indonesia", International Conference on Advanced Computer Science and Informatics, Bali, 20 - 21 November 2010\r\n\r\n"HMM Based Part-of-Speech Tagger for Bahasa Indonesia", Proceeding of the Fourth International MALINDO Workshop (MALINDO2010). Agustus 2010. Jakarta, Indonesia\r\n\r\n"Implementation of Indonesian Real Time Speech Dictation Application", Proceeding of ICACSIS (International Conference on COmputer Science and Information System) 2010. November 2010. Bali, Indonesia\r\n\r\n"Pemeriksaan Urutan Hafalan Al-Quran Memanfaatkan Pengenalan Suara Otomatis", Konferensi Nasional Informatika 2010, Institut Teknologi Bandung, 11 Oktober 2010\r\n\r\n"Evaluasi Otomatis Jawaban Persoalan Matematika", Konferensi Nasional Informatika 2010, Institut Teknologi Bandung, 11 Oktober 2010\r\n\r\n"Penggunaan Moodle untuk Knowledge Management System", Proceeding of “Konferensi Nasional Sistem Informasi (KNSI)” 2010, 22-23 Januari 2010, Palembang, Indonesia.\r\n\r\n"Deteksi OOV Menggunakan Hasil Pengenalan Suara Otomatis untuk Bahasa Indonesia", Journal of Jurnal Ilmu Komputer dan Informasi (JIKI), ISSN 1979-0732\r\n\r\n"Automated Essay Grading System using SVM and LSA for Essay Answers in Indonesian", Proceeding of International Conference on Advanced Computer Science and Information Systems, December 7th-8th, 2009, Jakarta, Indonesia.\r\n\r\n"Implementation of Indonesian Automated Speech Recognition for OOV Detection", Proceeding of International Conference on Advanced Computer Science and Information Systems, December 7th-8th, 2009, Jakarta, Indonesia.\r\n\r\n"INAGP: Pengurai Kalimat Bahasa Indonesia sebagai Alat Bantu untuk Pengembangan Aplikasi", Proceeding of “Seminar Nasional Ilmu Komputer dan Aplikasinya (SNIKA)”, Volume 4, No. 1, Oktober 2009, Bandung, Indonesia.\r\n"Information Extraction using SVM Uneven Margin for Multi Language Document", Proceedings of the Fourth International Conference on Information & Communication Technology and Systems (ICTS) 2008, ISSN 1858 1633 Surabaya - Indonesia, P249-253\r\n\r\n"Transitive Strategy on CLQA System for Low Resources Language Pair-via Development of Indonesia-Japanese CLQA", The Second International Malindo Workshop di Multimedia University (MMU), Cyberjaya, Selangor Malaysia, 12-13 Juni 2008\r\n\r\n"Indonesian-Japanese Transitive Translation using English for CLIR", Journal of Natural Language Processing, pp. 95-123, Volume 14 No 2, April 2007\r\n\r\n"A Machine Learning Approach for an Indonesian-English Cross Language Question Answering System", IEICE Transactions on Information and Systems, pp. 1841-1852, Volume E90-D No 11, November 2007\r\n\r\n"A Machine Learning Approach for Indonesian Question Answering System", Proceeding of the IASTED International Conference on Artificial Intelligence and Its Application (AIA 2007), 6 pages, 12-14 February 2007, Innsbruck, Austria.\r\n"Indonesian-Japanese CLIR using only Limited Resource", Proceeding of the Workshop on How Can Computational Linguistics Improve Information Retrieval, Workshop on ACL/Coling 2006, pp. 1-8, Sydney, Australia.\r\n\r\n"Query Transitive Translation using IR Score for Indonesian-Japanese CLIR”, Proceeding of Second Asia Information Retrieval Symposium, pp. 565-570, October 13-15 2005, Jeju Island, Korea."', '', 'Natural Language Processing; Speech Processing; Intelligent Tutoring System; Knowledge Management System', 'ayu3x4.jpg'),
(11, 20, 'M.M. Inggriani', '130796176', 'Undergraduate: Physics Engineering, Institute of Technology Bandung, Bandung-Indonesia\r\n\r\nMagister: Informatique, Institut National Polytechnique de Grenoble, Grenoble-France\r\n\r\nMagister: Informatique Double Competence, Diplome d''Etudes Superieures Specialicees, Universite Grenoble I - France\r\n\r\nDoctoral: Informatique, De L''Universite de Grenoble I, Grenoble-France', '"Automatic Source Code Plagiarism Detection", 10th ACIS International Conference on Software Engineering, Artificial Intelligence, Networking and Parallel/Distributed Computing (SNPD 2009), Catholic University of Daegu, Korea, May 27 - 29, 2009\r\n\r\n"A Conceptual Model of Indonesian Virtual Herbarium (IVH)", r-ICT Bandung Indonesia, 17-18 Juni 2009. ISBN : 978-979-15509-4-9\r\n\r\n"Pengembangan E-Learning dalam Pembelajaran Perubahan Keadaan Gas dan Termodinamika Kimia", SRITI 2008\r\n\r\n"An Application Generator Framelet", Seminar “The Ninth ACIS International Conference on Software Engineering, Artificial Intelligence, Networking, and Paralel/Distributed Computing (SNPD2008), Auguts 6-8, 2008, di Phuket, Thailand\r\n\r\n"Manajemen Perguruan Tinggi Berbasis Teknologi Informasi Sebagai Landasan Pengernbangan Badan Hukum Pendidikan Tinggi Yang Otonom", Seminar Nasional Manajemen Perguruan Tinggi menuju Otonomi Berbasis Teknologi Informasi, Denpasar (21 September 2007)\r\n\r\n"People Development in Software Engineering", Seminar: Role of Asia in Software Engineering in the Flat World, Yogyakarta (24 September 2007)\r\n\r\n"Experiential Based Learning in Software Engineering – Collaboration between University and Industry in Indonesia", Workshop on Software Engineering Education: Bridging The University/Industry Gap, Nagoya, Japan, 4 December 2007\r\n\r\n"Programming Exercises Via Algoshop: A Web Based Multiplayer Game", International Conference iiWAS 2006\r\n\r\n"Data Integration: An Experience of Information System Migration", International Conference iiWAS 2006\r\n\r\n"E-Learning di ITB: Fakta, Pengalaman & Harapan", Seminar on Pedagogy and E-Learning 2006\r\n\r\n"Pemanfaatan Teknologi Informasi di Negara Berkembang", Seminar Riset Teknologi Informasi SRITI 2006 (Keynotes Speech)\r\n\r\n"User Centered OSS Dissemination", Seminar Nasional Strategi Pemasyarakatan Open Source Software di Indonesia 2006', '', 'Programming', 'inge3x4.jpg'),
(12, 21, 'Gusti Ayu Putri Saptawati', '19650924199501200', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Finance & Information Systems, The University of New South Wales, New South Wales - Australia\r\n\r\nDoctoral: Electrical Engineering, Institut Teknologi Bandung, Bandung - Indonesia', '"ICT-based Approaches for Improving the Quality of Primary Education in Rural Areas", r-ICT Bandung Indonesia, 17-18 Juni 2009. ISBN : 978-979-15509-4-9.\r\n\r\n"e-Health Implementation for Rural Areas in Indonesia - Review on Data and Technology Readiness", r-ICT Bandung Indonesia, 17-18 Juni 2009. ISBN : 978-979-15509-4-9.\r\n\r\n"Penggunaan Teknologi Semantik untuk Pengembangan Sistem Penyimpanan Data yang Berintelejensi – Studi Kasus e-Health", e-Indonesia Initiative 2009 (eII2009) Konferensi dan Temu Nasional Teknologi Informasi dan Komunikasi untuk Indonesia 24 - 25 Juni 2009, Bandung.\r\n\r\n"Learning Bayesian Network Structure from Incomplete Data without any Assumption", International Conference on Database Systems for Advanced Application (DASFAA), New Delhi, Maret 2008\r\n\r\n"TPDA Algorithm for Learning BN Structure from Missing Value and Outliers in Data Mining", Jurnal Informatika Pusat Penelitian - 2006.\r\n\r\n"Exploration of Adaptive Boosting Ensemble Algorithm for Handling Concept Drifts in Mining Data Streams", The 2nd Information and Communication Technology Seminar - 2006.', '', 'Database; Data Mining & its Applications in e-Health', 'putri3x4.jpg'),
(13, 22, 'Mary Handoko W.', '19510707197803100', 'Undergraduate: Industrial Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Industrial Engineering, Institut Teknologi Bandung, Bandung - Indonesia', '', '', 'Project Management; Information System & Decision Support', 'mary3x4.jpg'),
(14, 23, 'Arry Akhmad Arman', '19650414199102100', 'Undergraduate: Electrical Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Electrical Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nDoctoral: Electrical Engineering, Institut Teknologi Bandung, Bandung - Indonesia', '"Review of e-Government Investment Valuation: A Survey", The 3rd International Conference on Chief Information Officer, Institut Teknologi Bandung, 5 - 7 Mei 2010\r\n\r\n"Sistem Pengenalan Wajah dengan Menggunakan Transformasi Wavelet Diskrit dan Support Vector Machine", Konferensi Nasional Informatika 2010, Institut Teknologi Bandung, 11 Oktober 2010\r\n\r\n"Alternatif Pembagian Kelas Kata (Part of Speech) Bahasa Indonesia untuk Proses Penterjemahan Secara Otomatis", e-Indonesia Initiative 2010 (eII2010) 5-7 Mei 2010, Bandung', '', '', 'arry akhmad3x4.jpg'),
(15, 24, 'Fazat Nur Azizah', '19770210200912200', '', '', '', 'Database (Modelling Data)\r\n', 'fazat3x4.jpg'),
(16, 25, 'Yani Widyani', '19700107199702200', 'Undergraduate: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia\r\n\r\nMagister: Informatics Engineering, Institut Teknologi Bandung, Bandung - Indonesia', '"Pengembangan Standar Dokumentasi untuk Pembangunan Perangkat Lunak Berorientasi Objek", Proc. Konferensi Nasional Sistem Informasi 2006', '', 'Software Engineering', 'yaniW3x4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembimbing_tugasakhir`
--

DROP TABLE IF EXISTS `pembimbing_tugasakhir`;
CREATE TABLE IF NOT EXISTS `pembimbing_tugasakhir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembimbing` int(11) NOT NULL,
  `id_tugasakhir` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pembimbing_ta` (`id_pembimbing`),
  KEY `ta_pembimbing` (`id_tugasakhir`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `pembimbing_tugasakhir`
--

INSERT INTO `pembimbing_tugasakhir` (`id`, `id_pembimbing`, `id_tugasakhir`) VALUES
(13, 7, 21),
(14, 14, 21),
(15, 16, 21),
(25, 9, 22),
(26, 12, 22),
(27, 13, 22),
(37, 9, 24),
(38, 11, 24),
(39, 13, 24),
(40, 6, 25),
(41, 10, 25),
(42, 14, 25),
(43, 5, 18),
(44, 6, 18),
(45, 15, 18),
(46, 8, 19),
(47, 9, 19),
(48, 11, 19),
(49, 5, 20),
(50, 10, 20),
(51, 14, 20),
(52, 6, 23),
(53, 10, 23),
(54, 15, 23);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `nama`) VALUES
(12, 'harga saham'),
(13, 'huffman'),
(14, 'graf'),
(15, 'graf berarah'),
(16, 'topologi'),
(17, 'jaringan'),
(18, 'server'),
(19, 'time series'),
(20, 'data'),
(21, 'deret waktu'),
(22, 'analisis'),
(23, 'algorithm'),
(24, 'malware'),
(25, 'encryption'),
(26, 'quarantine'),
(27, 'efisiensi'),
(28, 'garis'),
(29, 'simpul'),
(30, 'number'),
(31, 'material'),
(32, 'idustri'),
(33, 'industri'),
(34, 'pohon keputusan'),
(35, 'diagnosa'),
(36, 'kedokteran'),
(37, 'kriptografi visual'),
(38, 'modulo'),
(39, 'ascii');

-- --------------------------------------------------------

--
-- Table structure for table `tag_tugasakhir`
--

DROP TABLE IF EXISTS `tag_tugasakhir`;
CREATE TABLE IF NOT EXISTS `tag_tugasakhir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tugasakhir` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `t1` (`id_tugasakhir`),
  KEY `t2` (`id_tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `tag_tugasakhir`
--

INSERT INTO `tag_tugasakhir` (`id`, `id_tugasakhir`, `id_tag`) VALUES
(8, 21, 15),
(9, 21, 16),
(10, 21, 17),
(11, 21, 18),
(26, 22, 30),
(27, 22, 22),
(28, 22, 17),
(29, 22, 18),
(42, 24, 34),
(43, 24, 35),
(44, 24, 36),
(45, 25, 37),
(46, 25, 38),
(47, 25, 39),
(48, 18, 12),
(49, 18, 19),
(50, 18, 20),
(51, 18, 21),
(52, 18, 22),
(53, 19, 13),
(54, 19, 23),
(55, 19, 24),
(56, 19, 25),
(57, 19, 26),
(58, 20, 15),
(59, 20, 27),
(60, 20, 28),
(61, 20, 29),
(62, 23, 23),
(63, 23, 22),
(64, 23, 31),
(65, 23, 33);

-- --------------------------------------------------------

--
-- Table structure for table `tugasakhir`
--

DROP TABLE IF EXISTS `tugasakhir`;
CREATE TABLE IF NOT EXISTS `tugasakhir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` int(11) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `tahun` int(4) NOT NULL,
  `abstrak` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `link` varchar(128) NOT NULL,
  `poster` varchar(128) NOT NULL,
  `download` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mahasiswa_ta` (`id_mahasiswa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tugasakhir`
--

INSERT INTO `tugasakhir` (`id`, `id_mahasiswa`, `judul`, `tahun`, `abstrak`, `deskripsi`, `timestamp`, `link`, `poster`, `download`) VALUES
(18, 8, 'Hubungan Peramalan Indeks Harga Saham dengan Analisis Time Series', 2011, 'menjelaskan pemodelan data time series dalam peramalan harga saham', 'Pelaku transaksi modal harus mengambil keputusan di tengah tengah ketidakpastian harga saham. Untuk mengurangi resiko kesalahan pengambilan keputusan dapat melakukan prediksi harga saham dengan analisis time series. Makalah yang dibuat akan lebih menjelaskan gambaran umum secara teori mengenai pemodelan data time series.', '2013-06-12 14:41:44', 'Makalah Time Series.pdf', 'images.jpg', 1),
(19, 9, 'Huffman Algorithm for Antivirus Quarantine', 2012, 'huffman algorithm application', 'This paper is about using Huffman Algorithm \r\nfor Antivirus to secure malware from computer users. Since \r\nsystem of software is becoming more and more sophisticated, \r\nthe malware will try to innovate to keep their existence in the \r\nsystem. We need to secure the infected file so that user would \r\nnot be in danger. Quarantine is one solution to secure the \r\ninfected file. The best way of antivirus to do quarantine is by \r\nencrypting the infected file. By using Huffman algorithm in \r\nencrypting the infected file, we will get some benefits such as \r\ndata compression, safe encryption for the infected file, and a \r\nsmall running time.', '2013-06-12 15:42:38', 'Makalah-IF2091-2012-031.pdf', 'images (1).jpg', 0),
(20, 10, 'Aplikasi Graf Berarah Dalam Upaya Pengurangan Kemacetan', 2013, 'aplikasi graf berarah dalam struktur diskrit', '—Graf merupakan suatu bahasan yang sering \r\ndipakai karena dapat memodelkan suatu sistem tertentu\r\nseperti jaringan komunikasi, jaringan transportasi, dan \r\nlain-lain. Salah satu aplikasi graf dalam jaringan \r\ntransportasi selain pembuatan jadwal keberangkatan dan \r\nketibaan suatu kendaraan umum adalah dalam pembuatan \r\njalan dalam upaya mengurangi kemacetan.\r\nTujuan yang diinginkan dalam penanganan masalah \r\nperhubungan ini adalah menghubungkan tempat-tempat \r\nseefektif mungkin, baik dalam jarak yang ditempuh, waktu \r\nyang dibutuhkan, maupun biaya yang dikeluarkan. ', '2013-06-12 15:52:16', 'Makalah-IF2091-2012-015.pdf', 'download.jpg', 0),
(21, 11, 'Implementasi Graf berarah dalam Topologi Jaringan di  Perusahaan Distributor', 2012, 'pembahasan topologi jaringan dalam aplikasi graf berarah', 'Perkembangan yang sangat dinamis dari \r\nkomputasi beberapa tahun kebelakang telah memengaruhi \r\nperilaku pengguna komputer. Implementasi dari ilmu-ilmu \r\nyang bersifat diskrit banyak sekali dipakai dalam dunia \r\nteknologi informasi. Salah satunya adalah impelementasi \r\ngraf berarah untuk jaringan komputer. Dimulai dari sistem \r\njaringan yang kecil hingga sistem jaringan yang sangat \r\nbesar. Pemakaian metode ini sangat bermanfaat dalam \r\nmembangun sebuah sistem jaringan komputer. Makalah \r\nini menjelaskan secara umum pemakaian atau \r\nimplementasi dari graf berarahmenjadi sebuah sistem \r\njaringan, dan membahas topologi jaringan.', '2013-06-12 16:01:14', 'Makalah-IF2091-2012-027.pdf', 'download (1).jpg', 0),
(22, 12, 'Number Theory Application', 2010, 'number theory application', 'Number theory and its aplication, etc .', '2013-06-12 16:12:28', 'Makalah-IF2091-2012-015.pdf', 'images (2).jpg', 0),
(23, 13, 'Informatika dalam Perkembangan Industri Material', 2011, 'Perkembangan informatika dalam dunia industri material', 'Perkembangan informatika dalam dunia industri. Khususnya dalam bidang material.', '2013-06-12 16:19:48', 'Makalah-IF2091-2012-015.pdf', 'images (3).jpg', 0),
(24, 14, 'Diagnosa Ringan dengan Pohon Keputusan', 2011, 'Makalah ini akan membahas masalah kesehatan umum,  dengan menggunakan aplikasi pohon keputusan dan ilmu  kedokteran', 'Pohon sebagai cabang ilmu Struktur Diskrit, \r\nmemiliki keterkaitan dengan ilmu-ilmu lain dalam \r\nkehidupan kita. Seperti ilmu kedokteran misalnya. Dalam \r\nmenganalisis penyakit seorang pasien, secara tidak langsung \r\ndokter menggunakan ‘pola pikir’ pohon keputusan dalam \r\nmendiagnosa pasiennya. Dengan memperoleh kondisi pasien \r\nsebagai keputusan (simpul pohon sebagai keadaan) sampai \r\nakhirnya dapat mengerucut pada sebuah konklusi penyakit \r\ntertentu yang diderita pasien (daun pohon sebagai solusi). \r\nSehingga penyakit tersebut dapat diobati dengan cara \r\npengobatan yang tepat. \r\nMakalah ini akan membahas masalah kesehatan umum, \r\ndengan menggunakan aplikasi pohon keputusan dan ilmu \r\nkedokteran, sehingga kita sebagai orang awam mengenai \r\nilmu medis, dapat segera ‘mendiagnosa’ apa penyakit yang \r\nkita derita harus segera mendapat penanganan pihak medis, \r\natau dapat diobati sendiri. ', '2013-06-12 16:27:45', 'Makalah-IF2091-2012-080.pdf', 'Diagnosa_Ringan_dengan_Pohon_Keputusan.jpg', 0),
(25, 15, 'Kriptografi Visual Sederhana Berbasis Nilai Modulo pada  Intensitas Warna Gambar RGB ', 2013, 'implementasi Kriptografi Visual berdasarkan nilai Modulo  masing-masing intensitas warna pada gambar RGB. ', '—Pada makalah ini akan dijelaskan mengenai \r\nimplementasi Kriptografi Visual berdasarkan nilai Modulo \r\nmasing-masing intensitas warna pada gambar RGB. \r\nKombinasi nilai modulo ini kemudian dikonversi dalam \r\nheksadesimal yang kemudian diterjemahkan berdasarkan \r\ntable ASCII. Metode ini tidak akan menjadikan seseorang \r\ncuriga terhadap gambar yang kita kirim karena perubahan \r\nwarna yang dilakukan tidak signifikan, sehingga tidak \r\nterlihat sebagai noise. ', '2013-06-12 16:33:22', 'Makalah-IF2091-2012-038.pdf', 'Kriptografi_Visual_Sederhana_Berbasis_Nilai_Modulo_pada_Intensitas_Warna_Gambar_RGB.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `role` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `salt`, `role`) VALUES
(13, 'bernadettevina', '7ffe93d12205f84d89a6f9a469a00d5f', 'vivisutoyo@hotmail.com', '51b8131d755e28.78240767', 'mahasiswa'),
(14, 'benhardsitohang', 'd9eb4cc1e0146e0f32494bb8a2842b9b', 'benhard@gmail.com', '51b814ff8e1064.02175234', 'pembimbing'),
(15, 'tricya', '774a71816a584958cca195b756cd6cb1', 'cia@stei.itb.ac.id', '51b8164a8765d5.45408528', 'pembimbing'),
(16, 'rinaldi', '5de674325a87ce532d62f54b36fa6f80', 'rinaldi-m@stei.itb.ac.id', '51b81775863333.12044144', 'pembimbing'),
(17, 'saifulakbar', '06e956bad21fcb2a21119deb1d90c964', 'saiful@stei.itb.ac.id', '51b81869432d80.58697108', 'pembimbing'),
(18, 'rizasatria', 'e3dd27eacbd6a1abecc33f0786f6a144', 'riza@stei.itb.ac.id', '51b818f9ab1f90.14954443', 'pembimbing'),
(19, 'ayupurwarianti', 'fd00ddad309eef1a543085c3f3bcc1a5', 'ayu@stei.itb.ac.id', '51b81afc50f0b1.72348650', 'pembimbing'),
(20, 'inggriani', '6f8d2d0bebdd0032f4f81cdec67214e5', 'inge@stei.itb.ac.id', '51b81be20da9e1.85578665', 'pembimbing'),
(21, 'putrisaptawati', 'aea1bea51ba1ff8b1abfd2d1f942ca79', 'putri.saptawati@stei.itb.ac.id', '51b820b80cfd77.51500499', 'pembimbing'),
(22, 'maryhandoko', 'fe1824291113463dc1d4b28112bd1748', 'mary@informatika.org', '51b8214f35d3e7.96605036', 'pembimbing'),
(23, 'arryakhmad', '92852ad2e360c92d01ef6c8591b1b0f9', 'arman@stei.itb.ac.id', '51b821f3e4b2d4.76460096', 'pembimbing'),
(24, 'fazatnur', '2accb6bfe29dd31e85129c5765d5c0f7', 'fazat@stei.itb.ac.id', '51b82274ac7ae5.54729756', 'pembimbing'),
(25, 'yaniwidyani', 'f5f49ffa759567c1f56cf689fb10d75f', 'yani@stei.itb.ac.id', '51b82306b2f487.21132733', 'pembimbing'),
(26, 'sonnylazuardi', '6ed2c4ea989eff93ec538475088d4a28', 'sonnylazuardi@gmail.com', '51b832a15ae282.62768147', 'mahasiswa'),
(27, 'arinihasiana', '4a7a46cdbf8d8167d239b031bb37db05', '13511082@std.stei.itb.ac.id', '51b835f0823f04.34998510', 'mahasiswa'),
(28, 'farizan', 'ecd31d39cbbda6045cd82678e3bbaee9', '13511081@std.stei.itb.ac.id', '51b8375da96362.34286838', 'mahasiswa'),
(29, 'stellakurniawan', 'eae20f0f9dee238447b6ff8c122d40b8', '18211046@std.stei.itb.ac.id', '51b83a61293eb6.67469738', 'mahasiswa'),
(30, 'gumarus', 'fef79c464dd9702871a9c80ac6fbad13', '18211043@std.stei.itb.ac.id', '51b83c551dbd94.66827722', 'mahasiswa'),
(31, 'christian', '39539ba80acafa55190c63147f98ba00', 'christian_gunardi@hotmail.com', '51b83e4a8f0bd8.50630178', 'mahasiswa'),
(32, 'ignatiusevan', '08f30df718a40bb58a38e1902cb4cb05', 'ignatius.evan.d@s.itb.ac.id', '51b83f8e662f65.28851190', 'mahasiswa');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembimbing`
--
ALTER TABLE `pembimbing`
  ADD CONSTRAINT `user_pembimbing` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembimbing_tugasakhir`
--
ALTER TABLE `pembimbing_tugasakhir`
  ADD CONSTRAINT `pembimbing_ta` FOREIGN KEY (`id_pembimbing`) REFERENCES `pembimbing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ta_pembimbing` FOREIGN KEY (`id_tugasakhir`) REFERENCES `tugasakhir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tag_tugasakhir`
--
ALTER TABLE `tag_tugasakhir`
  ADD CONSTRAINT `t1` FOREIGN KEY (`id_tugasakhir`) REFERENCES `tugasakhir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t2` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tugasakhir`
--
ALTER TABLE `tugasakhir`
  ADD CONSTRAINT `mahasiswa_ta` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
