<?php
/**
 * Overload of CWebUser to set some more methods.
 */
class WebUser extends CWebUser
{
  /**
   * Mengambil data role dari user
   */
	public function getRole() {

  }
  /**
   * Mengambil profile picture user
   */
  public function getProfilePicture() {
    $model = $this->load;
    if ($model)
      return Yii::app()->baseUrl.'/images/'.$this->role.'/'.$model->foto;
    else 
      return null;
  }
  /**
   * Mengambil url untuk menampilkan profile
   */
  public function getProfileUrl() {
    $model = $this->load;
    if ($model)
      if ($this->role == 'mahasiswa')
        return Yii::app()->createUrl('/mahasiswa/view', array('id'=>$model->id));
      else if ($this->role == 'pembimbing')
        return Yii::app()->createUrl('/pembimbing/view', array('id'=>$model->id));
    else 
      return null;
  }

  /**
   * Mengembalikan id dari tabel mahasiswa atau pembimbing
   */
  public function getRealId() {
    $model = $this->load;
    if ($model)
      return $model->id;
    else 
      return null;
  }

  /**
   * Mengambil data user dari yang sedang login
   */
  public function getLoad() {
    $model = null;
    if ($this->role == 'mahasiswa')
      $model = Mahasiswa::model()->findByAttributes(array('id_user'=>$this->id));
    else if ($this->role == 'pembimbing')
      $model = Pembimbing::model()->findByAttributes(array('id_user'=>$this->id));
    return $model;
  }
  /**
   * Mengembalikan true jika role tersebut bisa mengedit
   */
  public function allowEdit($role, $id) {
    $model = $this->load;
    if ($model)
      return ($this->role === $role && $model->id === $id);
    else
      return false;
  }

  /**
   * Overrides a Yii method that is used for roles in controllers (accessRules).
   *
   * @param string $operation Name of the operation required (here, a role).
   * @param mixed $params (opt) Parameters for this operation, usually the object to access.
   * @return bool Permission granted?
   */
  public function checkAccess($operation, $params=array())
  {
      if (empty($this->id)) {
          // Not identified => no rights
          return false;
      }
      $role = $this->getState("role");
      if ($role === 'admin') {
        return true; // admin role has access to everything
      }
      // allow access if the operation request is the current user's role
      return ($operation === $role);
  }
}