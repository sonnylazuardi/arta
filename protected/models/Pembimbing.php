<?php

/**
 * This is the model class for table "pembimbing".
 *
 * The followings are the available columns in table 'pembimbing':
 * @property integer $id
 * @property integer $id_user
 * @property string $nama
 * @property string $nip
 * @property string $riwayat_pendidikan
 * @property string $publikasi
 * @property string $research
 * @property string $bidang_keahlian
 * @property string $foto
 *
 * The followings are the available model relations:
 * @property User $idUser
 * @property PembimbingTugasakhir[] $pembimbingTugasakhirs
 */
class Pembimbing extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pembimbing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembimbing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, nip', 'required'),
			array('nama, foto', 'length', 'max'=>128),
			array('nip', 'length', 'max'=>20),
			array('riwayat_pendidikan, publikasi, research, bidang_keahlian', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_user, nama, nip, riwayat_pendidikan, publikasi, research, bidang_keahlian, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'id_user'),
			'pembimbingTugasakhirs' => array(self::HAS_MANY, 'PembimbingTugasakhir', 'id_pembimbing'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'nama' => 'Nama',
			'nip' => 'Nip',
			'riwayat_pendidikan' => 'Riwayat Pendidikan',
			'publikasi' => 'Publikasi',
			'research' => 'Research',
			'bidang_keahlian' => 'Bidang Keahlian',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('riwayat_pendidikan',$this->riwayat_pendidikan,true);
		$criteria->compare('publikasi',$this->publikasi,true);
		$criteria->compare('research',$this->research,true);
		$criteria->compare('bidang_keahlian',$this->bidang_keahlian,true);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
   * Mengambil profile picuture user
  */
  public function getProfilePicture() {
    return Yii::app()->baseUrl.'/images/pembimbing/'.$this->foto;
  }
}