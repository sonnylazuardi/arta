<?php

/**
 * This is the model class for table "tugasakhir".
 *
 * The followings are the available columns in table 'tugasakhir':
 * @property integer $id
 * @property integer $id_mahasiswa
 * @property string $judul
 * @property integer $tahun
 * @property string $abstrak
 * @property string $deskripsi
 * @property string $timestamp
 * @property string $link
 * @property string $poster
 * @property integer $download
 *
 * The followings are the available model relations:
 * @property PembimbingTugasakhir[] $pembimbingTugasakhirs
 * @property TagTugasakhir[] $tagTugasakhirs
 * @property Mahasiswa $idMahasiswa
 */
class Tugasakhir extends CActiveRecord
{
	public $mytag;
	public $mypembimbing;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tugasakhir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tugasakhir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, tahun, abstrak, deskripsi', 'required'),
			array('id_mahasiswa, tahun, download', 'numerical', 'integerOnly'=>true),
			array('judul, link, poster', 'length', 'max'=>128),
			array('abstrak', 'length', 'max'=>255),
			array('poster', 'file', 'allowEmpty' => false, 'types' => 'jpg,jpeg,gif,png', 'maxSize' => 1024 * 800, 'on'=>'create'),
			array('link', 'file', 'allowEmpty' => false, 'types' => 'pdf', 'maxSize' => 1024 * 1024 * 5, 'on'=>'create'),
			array('poster', 'file', 'allowEmpty' => true, 'types' => 'jpg,jpeg,gif,png', 'maxSize' => 1024 * 800, 'on'=>'update'),
			array('link', 'file', 'allowEmpty' => true, 'types' => 'pdf', 'maxSize' => 1024 * 1024 * 5, 'on'=>'update'),
			array('mypembimbing', 'checkPembimbing', 'except' => 'download'),
			array('mytag', 'checkTags', 'except' => 'download'),
			array('id, id_mahasiswa, judul, tahun, abstrak, deskripsi, timestamp, link, poster, download', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pembimbingTugasakhir' => array(self::HAS_MANY, 'pembimbingTugasakhir', 'id_tugasakhir'),
			'pembimbing' => array(self::HAS_MANY, 'Pembimbing', 'id_pembimbing', 'through'=>'pembimbingTugasakhir'),
			'tags' => array(self::MANY_MANY, 'Tag', 'tag_tugasakhir(id_tugasakhir, id_tag)'),
			'penulis' => array(self::BELONGS_TO, 'Mahasiswa', 'id_mahasiswa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_mahasiswa' => 'Id Mahasiswa',
			'judul' => 'Judul',
			'tahun' => 'Tahun',
			'abstrak' => 'Abstrak',
			'deskripsi' => 'Deskripsi',
			'timestamp' => 'Timestamp',
			'link' => 'File Dokumen',
			'poster' => 'Poster',
			'download' => 'Download',
			'mytag' => 'Tag',
			'mypembimbing' => 'Pembimbing',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_mahasiswa',$this->id_mahasiswa);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('abstrak',$this->abstrak,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('poster',$this->poster,true);
		$criteria->compare('download',$this->download);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Mengembalikan URL poster untuk tugas akhir yang aktif
	 */
	public function getPosterUrl()
	{
		return Yii::app()->baseUrl.'/images/poster/'.$this->poster;
	}
	/**
	 * Mengembalikan URL link pdf untuk tugas akhir yang aktif
	 */
	public function getLinkUrl()
	{
		return Yii::app()->baseUrl.'/upload/pdf/'.$this->link;
	}
	/**
	 * Mengembalikan true jika TA ini bisa diedit
	 */
	public function allowEdit()
	{
		return (Yii::app()->user->realId == $this->penulis->id && Yii::app()->user->role == 'mahasiswa');
	}
	/**
	 * Melakukan aksi sebelum menyimpan
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->timestamp = date("Y-m-d H:i:s");
				$this->id_mahasiswa = Yii::app()->user->realId;
				$this->download = 0;
			}
			return true;
		}
		else
			return false;
	}

	public function behaviors(){
  	return array( 'CAdvancedArBehavior' => array(
    'class' => 'CAdvancedArBehavior'));
  }

	public function checkTags($mytag)
	{
		$datas = explode(',', $this->mytag);
		$hasil = array();
		foreach ($datas as $item) {
			$item = trim($item);
			if (!empty($item)) {
				$ret = Tag::model()->findByAttributes(array('nama'=>$item));
				if ($ret == null) {
					$new = new Tag;
					$new->nama = strtolower($item);
					$new->save();
					$hasil[] = $new->id;
				} else {
					if (!in_array($ret->id, $hasil))
						$hasil[] = $ret->id;
				}
			}
		}
		if (count($hasil) == 0)
			$this->addError('mytag', 'Tag harus diisi');
		else
			$this->tags = $hasil;
	}

	public function checkPembimbing($mypembimbing)
	{
		if (count($this->mypembimbing) > 3)
			$this->addError('mypembimbing', 'Dosen Pembimbing terlalu banyak maksimal 3 dosen pembimbing');
		else if (count($this->mypembimbing) == 0)
			$this->addError('mypembimbing', 'Anda harus memasukkan Dosen pembimbing minimal 1');
		// else
		// 	$this->pembimbing = $this->mypembimbing;
	}

	public function savePembimbing()
	{
		PembimbingTugasakhir::model()->deleteAll('id_tugasakhir=:i', array(':i'=>$this->id));
		foreach ($this->mypembimbing as $key => $value) {
			$model = new PembimbingTugasakhir();
			$model->id_pembimbing = $value;
			$model->id_tugasakhir = $this->id;
			$model->posisi = $key+1;
			$model->save(false);
		}
	}

	public function loadTags()
	{
		$arr = array();
    foreach ($this->tags as $data) {
      $arr[] = $data->nama;
    }
    $this->mytag = implode(', ', $arr);
	}
	public function loadPembimbing()
	{
		$arr = array();
		$model = PembimbingTugasakhir::model()->findAll('id_tugasakhir = :i order by posisi asc',array(':i'=>$this->id));
    foreach ($model as $data) {
      $arr[$data->posisi] = $data->pembimbing->id;
    }
    $this->mypembimbing = $arr;
	}
}
