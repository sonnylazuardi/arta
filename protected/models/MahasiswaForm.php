<?php
/**
 * Register form model
 */
class MahasiswaForm extends CFormModel
{
	public $username;
	public $password;
	public $password2;
	public $email;
	public $nama;
	public $nim;
	public $foto;
	public $verifyCode;
	
	public function rules()
	{
		return array(
			array('username', 'match', 'allowEmpty' => false, 'pattern' => '/[A-Za-z0-9\x80-\xFF]+$/'),
			array('username', 'length', 'max'=>50),
			array('email', 'email'),
			array('email, username', 'unique', 'className' => 'User' ),
			array('username, password, password2, email, nama, nim', 'required'),
			array('nama', 'length', 'max'=>100),
			array('username, password, password2', 'length', 'min' => 6, 'max' => 32),
			array('password2', 'compare', 'compareAttribute'=>'password'),
			array('email', 'length', 'min' => 3, 'max' => 55),
			array('foto', 'file', 'allowEmpty' => true, 'types' => 'jpg,jpeg,gif,png', 'maxSize' => 1024 * 1024 * 2),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}
	
	/**
	 * Attribute values
	 *
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Username',
			'password' => 'Password',
			'password2' => 'Ulangi Password',
			'email' => 'Email',
			'nama' => 'Nama',
			'nim' => 'NIM',
			'picture' => 'Foto',
		);
	}
	
}