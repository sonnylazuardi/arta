<?php

/**
 * This is the model class for table "pembimbing_tugasakhir".
 *
 * The followings are the available columns in table 'pembimbing_tugasakhir':
 * @property integer $id
 * @property integer $id_pembimbing
 * @property integer $id_tugasakhir
 * @property integer $posisi
 *
 * The followings are the available model relations:
 * @property Pembimbing $idPembimbing
 * @property Tugasakhir $idTugasakhir
 */
class PembimbingTugasakhir extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PembimbingTugasakhir the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembimbing_tugasakhir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pembimbing, id_tugasakhir', 'required'),
			array('id_pembimbing, id_tugasakhir, posisi', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_pembimbing, id_tugasakhir, posisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pembimbing' => array(self::BELONGS_TO, 'Pembimbing', 'id_pembimbing'),
			'tugasakhir' => array(self::BELONGS_TO, 'Tugasakhir', 'id_tugasakhir'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pembimbing' => 'Id Pembimbing',
			'id_tugasakhir' => 'Id Tugasakhir',
			'posisi' => 'Posisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pembimbing',$this->id_pembimbing);
		$criteria->compare('id_tugasakhir',$this->id_tugasakhir);
		$criteria->compare('posisi',$this->posisi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}